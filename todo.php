<?php

$todos = [];

if(file_exists("todo.txt")){
    $file = file_get_contents("todo.txt");
    $todos = unserialize($file);
}

if(isset($_POST['todo'])) {
    $data = $_POST['todo'];
    $todos[] = [
        'todo'=> $data,
        'status' =>0
    ];
    $daftar_belanja = serialize($todos);
    file_put_contents("todo.txt",$daftar_belanja);
    header("location:todo.php");
}
if(isset($_GET['status'])) {
    $todos[$_GET['key']]['status'] = $_GET['status'];
    $daftar_belanja = serialize($todos);
    file_put_contents("todo.txt",$daftar_belanja);
    header("location:todo.php");
}

if(isset($_GET['id'])) {
    unset($todos[$_GET['id']]);
    $daftar_belanja = serialize($todos);
    file_put_contents("todo.txt",$daftar_belanja);
    header("location:todo.php");
}
// echo "<pre>";
// print_r($todos);
// echo "</pre>";
?>

<h2>Todo Apps</h2>
<form action="" method="post">
    <label>Daftar Belanja Hari ini</label><br>
    <input type="text" name="todo">
    <button type="submit">Simpan</button>
</form>
<ul>
    <?php foreach($todos as $key => $value):?>
    <li>
        <input type="checkbox" name="todo" onclick="window.location.href='todo.php?status=<?php echo ($value['status']==0) ? '1':'0';?>&key=<?php echo $key;?>'" <?php echo ($value['status']==1) ? "checked": "";?>>
        <label><?php 
        if ($value['status']==0) {
        echo $value['todo'];
        } else {
        echo "<del>".$value['todo']."</del>";
    }    ?>
    </label>
        <a href="todo.php?id=<?php echo $key;?>">Hapus</a>
    </li>
    <?php endforeach; ?>
</ul>
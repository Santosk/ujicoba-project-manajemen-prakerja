<?php
include "koneksi.php";
if(isset($_GET['no_prakerja'])) {
    $no_prakerja = $_GET['no_prakerja'];

    //proses query untuk penghapusan
    $q = mysqli_query($koneksi,"delete from t_prakerja where no_prakerja='$no_prakerja'");

    if ($q) {
        echo "<script>alert('Hapus Data Berhasil!');window.location.href='index.php'</script>";
    }
}

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/owl.carousel.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- Style -->
    <link rel="stylesheet" href="css/style.css">

    <title>Manajemen Data Prakerja</title>
  </head>
  <body>
  

  <div class="content">
    
    <div class="container">
      <h2 class="mb-5">Manajemen CRUD Data  Prakerja</h2>
      <a class="btn btn-primary"  role="button" href="tambah.php">+ Tambah Data</a>


      <div class="table-responsive custom-table-responsive">
      <form action="" method="post">
            <table class="table custom-table">
                <thead>
                    <tr>
                        <th>Silakan isi data yang anda cari</th>
                        <th>:</th>
                        <th><input type="text" name="cari">
                    <button type="submit" class="btn btn-primary" btn-lg btn-block>Cari</button></th>
                    </tr>
                </thead>
            </table>
        </form>
        <table class="table custom-table">
          <thead>
            <tr>  

            <th>No.</th>
                <th>No. Kartu Prakerja</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Opsi</th>
            </tr>
          </thead>
          <tbody>
          <?php
                if(isset($_POST['cari'])) {
                    $cari = $_POST['cari'];
                    $data = mysqli_query($koneksi,"select * from t_prakerja where 
                    no_prakerja like '%$cari%' OR 
                    nama like '%$cari%' OR 
                    alamat like '%$cari%'");
                } else {
                    $data = mysqli_query($koneksi,"select * from t_prakerja");
                }
                $no=1;
                while ($r = mysqli_fetch_array($data)) :
                ?>
                <tr?><td><?php echo $no;?></td>
                <td><?php echo $r['no_prakerja'];?></td>
                <td><?php echo $r['nama'];?></td>
                <td><?php echo $r['alamat'];?></td>
                <td><a  class="btn btn-success"  role="button" href="ubah.php?no_prakerja=<?php echo $r['no_prakerja'];?>">ubah</a> 
                | <a  class="btn btn-danger"  role="button" href="javascript:hapusData('index.php?no_prakerja=<?php echo $r['no_prakerja'];?>','<?php echo $r['nama'];?>')">Hapus</a></td>
                </tr>
                <?php 
            $no++;
            endwhile;?>
            
          </tbody>
        </table>
      </div>


    </div>

  </div>
    
    

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script>
    function hapusData(urlHapus,data) {
        if(confirm("Apakah anda yakin menghapus data atas nama "+data+"?")) {
            window.location.href= urlHapus;
        }
    }
</script>
  </body>
</html>

/*
 Navicat Premium Data Transfer

 Source Server         : lokal
 Source Server Type    : MySQL
 Source Server Version : 50740
 Source Host           : localhost:3306
 Source Schema         : prakerjadb

 Target Server Type    : MySQL
 Target Server Version : 50740
 File Encoding         : 65001

 Date: 05/04/2024 21:20:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_prakerja
-- ----------------------------
DROP TABLE IF EXISTS `t_prakerja`;
CREATE TABLE `t_prakerja`  (
  `no_prakerja` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jenis_kelamin` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`no_prakerja`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_prakerja
-- ----------------------------
INSERT INTO `t_prakerja` VALUES ('123', 'Ade', 'Bandung', NULL);
INSERT INTO `t_prakerja` VALUES ('234', 'Khalid', 'Medan', NULL);
INSERT INTO `t_prakerja` VALUES ('345', 'Muttaqin', 'Semarang', NULL);
INSERT INTO `t_prakerja` VALUES ('464', 'Kuwat Santoso', 'Bandung', NULL);

SET FOREIGN_KEY_CHECKS = 1;

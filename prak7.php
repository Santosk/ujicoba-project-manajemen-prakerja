<?php

function perkenalan($nama, $thn_lahir, $salam="Assalmu'alaikum") {
    echo "$salam,<br>";
    echo "Perkenalkan , nama saya $nama<br>";
    echo "Senang berkenalan dengan Anda<br>";
    echo "Umur saya adalah ". hitungUmur($thn_lahir,date('Y'))." tahun";
    echo "<hr>";
}

function hitungUmur($thn_lahir, $thn_skrng){
    $umur = $thn_skrng - $thn_lahir;
    echo $umur;
}

perkenalan("Ade",2000,"Selamat pagi");
perkenalan("Khalid",2001,"Selamat Siang");
perkenalan("Muttaqien",2002);

